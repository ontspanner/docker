#!/bin/sh

exit_fatal() {
  echo "$0: fatal: $1" >&2
  exit 1
}

CONTENT_DIR='/srv/static'
CONF_DIR='/etc/ontspanner'

# Ensure data volumes are mounted
[[ -d "${CONTENT_DIR}" ]] || exit_fatal 'static content directory does not exist'
[[ -d "${CONF_DIR}" ]]    || exit_fatal 'configuration directory does not exist'

# Ensure all mounts are read-only
touch "${CONTENT_DIR}/test" 2>/dev/null && exit_fatal 'static content directory must not be writable'
touch "${CONF_DIR}/test"    2>/dev/null && exit_fatal 'configuration directory must not be writable'

if [[ "$1" == 'shell' ]]; then
  # Start a shell for testing purposes
  exec sh
fi

# Start monit; monit will take care of starting the cron daemon and web server
exec monit -Id 30
