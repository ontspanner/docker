#!/bin/sh

APP_DIR="`realpath "$0"`"
APP_DIR="`dirname "${APP_DIR}"`"
TEST_DIR="${APP_DIR}/test"
EXTRA_ARGS=''
CMD=''

[[ "$1" == 'shell' ]] && EXTRA_ARGS='-it'

sudo docker run --rm \
  -v "${TEST_DIR}/conf:/etc/ontspanner:ro,Z" \
  -v "${TEST_DIR}/static:/srv/static:ro,Z" \
  ${EXTRA_ARGS} \
  ontspanner \
  $@
