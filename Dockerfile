FROM alpine:3.6

# Install required system and Python packages; also clean up default nginx configuration and redirect /var/run to /run.
RUN apk add --no-cache make monit nginx python2 su-exec tini \
    && python -m ensurepip \
    && pip2 install --upgrade pip setuptools \
    && find /etc/nginx/ -mindepth 1 -maxdepth 1 ! -name mime.types -exec rm -rf {} + \
    && rm -rf /etc/init.d/* /usr/lib/python*/ensurepip /var/run \
    && ln -s /run /var/run

# Copy backend and configuration files.
COPY ext/backend /tmp/backend
COPY src /

# Fix configuration file permissions.
RUN chmod 0500 /etc/init.d/start.sh \
    && chmod 0400 /etc/monitrc \
    && chmod 0500 /etc/nginx \
    && chmod 0400 /etc/nginx/*

# Set up cron daemon: remove defaults and redirect logging.
RUN crontab -u root /tmp/root.crontab \
    && rm -rf /etc/periodic \
    && ln -sf /proc/1/fd/2 /var/log/crond.log

# Set up nginx: redirect logging
RUN ln -sf /proc/1/fd/1 /var/log/nginx/access.log \
    && ln -sf /proc/1/fd/2 /var/log/nginx/error.log \
    && rm -rf /var/www

# Set up our application (see also src/tmp/root.crontab).
RUN adduser -h /srv/data/calendar -s /bin/false -SDH calendar \
    && make -C /tmp/backend install DESTDIR=/usr \
    && install -dm 0555 /srv/data \
    && install -do calendar -g www-data -m 0750 /srv/data/calendar

# Finalise image by uninstalling the package manager and marking most directories as read-only.
RUN pip2 uninstall -qy pip setuptools \
    && apk del --no-cache --purge alpine-keys apk-tools libc-utils make musl-utils \
    && rm -rf /root/.cache /tmp/* \
    && chmod -R a-w bin etc home lib media mnt root sbin usr

# Expose only port 80; SSL termination must be done outside this docker container.
EXPOSE 80

ENTRYPOINT ["/sbin/tini", "--", "/etc/init.d/start.sh"]
