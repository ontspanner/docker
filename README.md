# Fotoclub Ontspanner Lebbeke -- Website Docker image

This repository contains the sources to build a Docker image that contains the backend and a web server to host the
static content of the site. The image will not contain:

- the configuration of the backend;
- the static content of the web site.


## Architecture

The image is based on Alpine Linux, a distribution with a very small Docker image size of around 5 MB. It runs two
core services: a web server (`nginx`), and a cron daemon (`busybox` `crond`). The web server's main responsibility is to
serve static content. The cron daemon periodically calls our backend. Our backend scripts are run deprivileged using
su-exec, a C reimplementation of gosu.

Docker images need a single entry point. We therefore use `monit` to run and monitor our services. As `monit` is not
designed to run as `init` process, we use `tini` as entry point, and let `tini` run `monit`. This configuration ensures
that zombie processes are reaped as needed, and that SIGTERM (`docker stop`) is handled correctly.

All logging in the image is redirected to the standard output and error streams of the `init` process (PID 1) so that
we receive all logs in the Docker log collector. We can use `/dev/stdout` and `/dev/stderr` for `tini` and `monit`, but
not for our services launched by `monit` as the latter messes up those file descriptors. We therefore trick our services
into using the file descriptors of PID 1 by using `/proc/1/fd/1` and `/proc/1/fd/2` as replacements for `/dev/stdout`
and `/dev/stderr` respectively.


## Building

The `backend` repository must be checked out at `ext/backend` in order to build the image; then run `build.sh`. In order
to use a copy of the repository in a different location, you _must_ use a bind mount, as symbolic links are not
supported by the docker build process; e.g.:

```sh
sudo mount --bind /other/path/to/backend ./ext/backend
```


## Running

To supply the container with the necessary files, the following volumes must be mounted read-only:

- configuration files at `/etc/ontspanner`;
- static content at `/srv/static`.

This can be done using the following command:

```sh
docker run -v CONFIG_DIR:/etc/ontspanner:ro,Z -v CONTENT_DIR:/srv/static:ro,Z ontspanner
```

The included script `test.sh` can be used to create a container to run the image. Use `test.sh shell` to start an
interactive shell instead of `monit`.
