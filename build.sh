#!/bin/sh

APP_DIR="`realpath "$0"`"
APP_DIR="`dirname "${APP_DIR}"`"
BACKEND_DIR="${APP_DIR}/ext/backend"

exit_fatal() {
  echo "$0: fatal: $1" >&2
  exit 1
}

# Ensure backend directory exists
[[ -d "${BACKEND_DIR}" ]] || exit_fatal "backend must be checked out at '${BACKEND_DIR}'"

# Make version string
DOCKER_GIT_COMMIT="$(git log -1 --format='%h')" || exit_fatal 'failed to determine HEAD commit of docker repository'
DOCKER_GIT_DIRTY="$(git status --porcelain)" || exit_fatal 'failed to determine status of docker repository'
if [[ -n "${DOCKER_GIT_DIRTY}" ]]; then
  DOCKER_GIT_COMMIT+="_DIRTY"
fi
BACKEND_GIT_COMMIT="$(git -C "${BACKEND_DIR}" log -1 --format='%h')" || exit_fatal 'failed to determine HEAD commit of backend repository'
BACKEND_GIT_DIRTY="$(git -C "${BACKEND_DIR}" status --porcelain)" || exit_fatal 'failed to determine status of backend repository'
if [[ -n "${BACKEND_GIT_DIRTY}" ]]; then
  BACKEND_GIT_COMMIT+="_DIRTY"
fi

VERSION="$(date -u +%Y%m%d%H%M%S)-DOCKER_${DOCKER_GIT_COMMIT}-BACKEND_${BACKEND_GIT_COMMIT}" || exit_fatal 'failed to format version string'

# Build the image; use --force-rm to always delete intermediate images
sudo docker build --force-rm -t "ontspanner:${VERSION}" -t ontspanner:latest "${APP_DIR}" || exit_fatal 'failed to build docker image'
